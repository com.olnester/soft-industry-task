**A quick guide of using the test framework.**

In order to prepare your Intellij Idea for work please **setup the next libs:** 

- jdk 8+ and JAVA_HOME
- Maven
- Install plugins for Intellij Idea: Gherkin, Cucumber for Java
- Navigate to Intellij Idea Settings -> Editor -> File types -> select Cucumber scenario -> add ***.feature** file name 
  pattern if it doesn't exist and save changes
- Setup Gitlab-runner or use existing one. More details https://gitlab.com/com.olnester/product-market/-/settings/ci_cd

**To Run tests locally:**

    mvn clean verify

**Test report can be found here:**

    target/site/serenity/index.html

**To run pipeline on Gitlab:**
1. Navigate to https://gitlab.com/com.olnester/soft-industry-task/-/pipelines
2. Click on "Run pipeline" button.
3. To see the **test report** you need:
  - click on created pipeline
  - Click on "report" job
  - Find the link under section: "Test report can be found here"
like this: 
  
  https://com.olnester.gitlab.io/-/soft-industry-task/-/jobs/3305886338/artifacts/target/site/serenity/index.html

**To add new tests:**

1. Add new Feature file to the resources/features directory
2. Create StepDefinitions class in stepdefinitions package with valid name
3. Describe steps StepDefinitions class according feature file from step 1 
4. Describe json as dto object and place it to dto package if necessary
5. Describe all steps in steps package
6. Use ProjectProperties to store resources
7. To add new stage or update existing one in pipeline use .gitlab-ci.yml 

**Note:** As result of test execution was found a bug:

Summary: "Get products by "water" as product name returns product with "spa touch niet bruisend raspberry apple" in 
title. 
Expected: should not be found apple, tofu and mango in title". For more details, take a look at Test report on 
gitlab

**What was refactored:**
1. The structure of test classes
2. Added properties
3. Updated pom.xml
4. Updated .feature test file
5. Added .gitlab-ci.yml