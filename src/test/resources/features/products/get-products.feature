Feature: Get product by name

  Scenario Outline: Get product by name
    When I make call to get product by <Product Name>
    Then The status code should be <Status code> and title <Product Name>
    Examples:
      | Product Name | Status code |
      | apple     | 200 |
      | mango     | 200 |
      | tofu     | 200 |

  Scenario Outline: Get product by invalid product
    When I make call to get invalid product <Product Name>
    Then The status code should be <Status Code> and error message '<Message>'
    Examples:
      | Product Name | Status Code | Message   |
      | car          | 404         | Not found |


  Scenario Outline: Get product by name "water"
    When I make call to get product by water
    Then The status code should be <Status Code> and title doesn't contain <Product Name>
    Examples:
      | Product Name | Status Code |
      | apple     | 200 |
      | mango     | 200 |
      | tofu     | 200 |