package starter.steps;

import static starter.utils.ProjectProperties.PRODUCTS;

import java.util.HashMap;
import java.util.Map;
import net.thucydides.core.annotations.Step;
import starter.BaseEndpoint;

public class GetProductApi extends BaseEndpoint {

    @Step("Get product by name {0}")
    public void getProductByName(String productName) {
        Map<String, String> params = new HashMap<>();
        params.put("productName", productName);
        get(PRODUCTS, params);
    }
}
