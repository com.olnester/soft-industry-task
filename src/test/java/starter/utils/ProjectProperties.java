package starter.utils;

import net.thucydides.core.util.SystemEnvironmentVariables;

public class ProjectProperties {

    public static final String BASE_URL = getProperty("environments.default.base.url");
    public static final String PRODUCTS = getProperty("products");

    private static String getProperty(String propName) {
        return SystemEnvironmentVariables.createEnvironmentVariables().getProperty(propName);
    }
}
