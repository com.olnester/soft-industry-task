package starter.stepdefinitions;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.Arrays;
import net.thucydides.core.annotations.Steps;
import starter.dto.ErrorResponse;
import starter.dto.ProductDto;
import starter.steps.GetProductApi;

public class GetProductsStepDefinitions {

    @Steps
    GetProductApi getProductApi;

    @When("I make call to get product by {word}")
    public void makeCallToGetProductBy(String productName) {
        getProductApi.getProductByName(productName);
    }

    @Then("The status code should be {int} and title {word}")
    public void theStatusCodeShouldBeStatusCodeAndTitleProductName(int statusCode, String productName) {
        restAssuredThat(response -> response.statusCode(statusCode));
        restAssuredThat(response -> Arrays.asList(response.extract().as(ProductDto[].class))
            .forEach(x -> assertThat(x.getTitle().toLowerCase()).contains(productName)));
    }

    @When("I make call to get invalid product {word}")
    public void iMakeCallToGetInvalidProduct(String productName) {
        getProductApi.getProductByName(productName);
    }

    @Then("The status code should be {int} and error message {string}")
    public void theStatusCodeShouldBe(int statusCode, String errorMessage) {
        restAssuredThat(response -> response.statusCode(statusCode));
        restAssuredThat(
            response -> assertThat(response.extract().as(ErrorResponse.class).getDetail().getMessage()).isEqualTo(
                errorMessage));
    }

    @Then("The status code should be {int} and title doesn't contain {word}")
    public void theStatusCodeShouldBeStatusCodeAndTitleDoesnTContainProductName(int statusCode, String productName) {
        restAssuredThat(response -> response.statusCode(statusCode));
        restAssuredThat(response -> Arrays.asList(response.extract().as(ProductDto[].class))
            .forEach(x -> assertThat(x.getTitle().toLowerCase()).doesNotContain(productName)));
    }
}
