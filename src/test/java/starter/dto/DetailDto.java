package starter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DetailDto {

    @JsonProperty(value = "error")
    private boolean error;
    @JsonProperty(value = "message")
    private String message;
    @JsonProperty(value = "requested_item")
    private String requestedItem;
    @JsonProperty(value = "served_by")
    private String servedBy;

    public DetailDto() {
    }
}
