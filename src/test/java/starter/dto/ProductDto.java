package starter.dto;

import lombok.Data;

@Data
public class ProductDto {

    private String provider;
    private String title;
    private String url;
    private String brand;
    private float price;
    private String unit;
    private Boolean isPromo;
    private String promoDetails;
    private String image;

}
