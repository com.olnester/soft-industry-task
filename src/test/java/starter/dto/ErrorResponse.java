package starter.dto;

import lombok.Data;

@Data
public class ErrorResponse {

    private DetailDto detail;
}
