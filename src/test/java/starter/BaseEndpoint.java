package starter;

import static starter.utils.ProjectProperties.BASE_URL;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import java.util.Map;
import net.serenitybdd.rest.SerenityRest;

public class BaseEndpoint {

    static {
        RestAssured.baseURI = BASE_URL;
    }

    public void get(String resource, Map<String, String> pathParam) {
        SerenityRest.given()
            .log().all()
            .contentType(ContentType.JSON)
            .pathParams(pathParam)
            .get(resource);
    }
}
